﻿
function deleteRecord(successCallback) {
    Swal.fire({
        title: 'Are you sure?',
        text: 'You will not be able to recover this Record!',
        type: 'error',
        showCancelButton: true,
        customClass: {
            confirmButton: 'btn btn-danger btn-lg',
            cancelButton: 'btn btn-default btn-lg'
        }
    }).then((result) => {
        successCallback(result.value);
    })
}


