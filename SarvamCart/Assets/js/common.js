﻿new WOW({ mobile: false }).init();
$(function () {
    new WOW().init();
});
$(function () {
    $(window).on("scroll", function () {

        if ($(window).scrollTop() > 50) {

            $("header").addClass("header-bg");
        } else {
            $("header").removeClass("header-bg");
        }
    });

});
$(window).on('load', function () { // makes sure the whole site is loaded
    $('#status').fadeOut(); // will first fade out the loading animation
    $('#preloader').delay(1000).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $('body').delay(1000).css({
        'overflow': 'visible'
    });
});
$(document).on('click', '.qty-plus', function () {
    $(this).prev().val(+$(this).prev().val() + 1);
});
$(document).on('click', '.qty-minus', function () {
    if ($(this).next().val() > 0) $(this).next().val(+$(this).next().val() - 1);
});