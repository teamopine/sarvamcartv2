﻿using SarvamCart.Domain.Models;
using SarvamCart.Domain.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SarvamCart.Controllers
{
    public class UserCartController : BaseController
    {
        private readonly IQuotationService QuotationService;
        private readonly IQuotationDetailService QuotationDetailService;
        private readonly IProductImageService ProductImageService;
        private readonly Models.UserManager UserManager;
        private readonly IMailService MailServices;

        public UserCartController(IQuotationService _quotationServices, UserService _userservice, IQuotationDetailService _quotationDetailService, IProductImageService _ProductImageService, Models.UserManager _userManager, IMailService _mailServices)
        {
            QuotationService = _quotationServices;
            UserManager = _userManager;
            ProductImageService = _ProductImageService;
            QuotationDetailService = _quotationDetailService;
            MailServices = _mailServices;
        }
        // GET: Cart
        public ActionResult Index()
        {
            return View();
        }
        //public ActionResult AddToCart()
        //{
        //    return RedirectToAction("CartList");
        //}

        [HttpPost]
        public ActionResult AddToCart(int productId)
        {
            //product will add in cart

            var UserId = UserManager.UserDetail.UserID ?? 0;
            if (UserId == 0)
            {
                TempData["error"] = "you need to login first";
                return Json(new { IsSuccess = false });
            }
            var userCart = QuotationService.FirstOrDefault(x => x.UserId == UserId && x.StatusOfOrder == 0);
            if (userCart == null)
                userCart = QuotationService.Add(new Domain.Data.Quotation { UserId = UserId, CreatedOn = DateTime.Now, StatusOfOrder = 0 });
            QuotationDetailService.Add(new Domain.Data.QuotationDetail { QuotationId = userCart.Id, ProductId = productId });
            QuotationDetailService.Save();
            this.cartCounter["SessionCartCounter"] = userCart.QuotationDetails.Count();
            var d = userCart.QuotationDetails.Count();
            // this.cartCounter["SessionCartCounter"] = QuotationService.FirstOrDefault(x => x.UserId == UserManager.UserDetail.UserID && x.StatusOfOrder == 0).QuotationDetails.Count();
            return Json(new { IsSuccess = true ,count= userCart.QuotationDetails.Count() });
        }
        public ActionResult CartList()
        {
            // list of cart list of login user

            ViewModelQuotaion viewModel = new ViewModelQuotaion();
            var UserId = UserManager.UserDetail.UserID ?? 0;
            var userCart = QuotationService.FirstOrDefault(x => x.UserId == UserId && x.StatusOfOrder == 0);
            //var result = CartService.FirstOrDefault(x => x.Id == userCart.Id);

            if (userCart != null)
            {
                var cartDetail = QuotationDetailService.FirstOrDefault(x => x.QuotationId == userCart.Id);
                viewModel.CartDetails = userCart.QuotationDetails.Select(x => new ManageQuotaionDetailModel
                {
                    Id = x.Id,
                    QuotationId = x.QuotationId ?? 0,
                    ProductId = x.ProductId ?? 0,
                    ProductName = x.Product.Title,
                    Quentity = Convert.ToInt32(x.Quentity ?? 0),
                    ProductImage = ProductImageService.FirstOrDefault(p => p.ProductId == x.ProductId && !p.IsDeleted).InnerImages
                }).ToList();
            }
            else
            {
                ViewBag.Message = "Your cart is empty!!";
            }

            this.cartCounter["SessionCartCounter"] = viewModel.CartDetails.Count();
            Session["SessionCartCounter"] = viewModel.CartDetails.Count();

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult CartList(ViewModelQuotaion viewModel)
        {
            // user is request for quotation
            var UserId = UserManager.UserDetail.UserID ?? 0;
            var userCart = QuotationService.FirstOrDefault(x => x.UserId == UserId && x.StatusOfOrder == 0);

            foreach (var data in viewModel.CartDetails)
            {
                var result = QuotationDetailService.FirstOrDefault(x => x.Id == data.Id);
                result.Quentity = data.Quentity;

                QuotationDetailService.Edit(result);
                QuotationDetailService.Save();
            }

            if (userCart != null)
            {
                userCart.StatusOfOrder = 1;
                QuotationService.Edit(userCart);
                QuotationService.Save();
            }

            // notify to the about quotation via email

            var body = "One quotation request!! Please check out that.";
            var to = "";
            var from = "";
            var subject = "";
            var html = "";
            to = ConfigurationManager.AppSettings["AdminMailID"];
            from = userCart.User.EmailAddress;
            subject = "You got new quotation request!!";
            html = "<div style='border:1px solid ; width:80%;'><div class='leave' style='margin:0 10% 0 10%;'><p>Name:" + userCart.User.UserName + "<br>" + "Contact Number:" + userCart.User.ContactNumber +
                "<br>" + "Date:" + DateTime.Now +
                      "<p> " + body + "</p>";
            // +"</p><br> <br> " + viewModel.Name + " <br><br></div></div>";
            MailServices.Send(from, to, subject, html);
            if (viewModel != null)
            {
                TempData["Msg"] = "your quotaion request is send successfully";
            }

            return RedirectToAction("CartList", "UserCart");
        }

        [HttpPost]
        public ActionResult DeleteCartDetail(int cartDetailId)
        {
            var data = QuotationDetailService.FirstOrDefault(x => x.Id == cartDetailId);
            if (data != null)
            {
                QuotationDetailService.Delete(data);
                QuotationDetailService.Save();
                return Json(new { IsSuccess = true });
            }
            return View();
        }
    }
}