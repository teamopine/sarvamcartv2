﻿using Rotativa;
using Rotativa.Options;
using SarvamCart.Domain.Models;
using SarvamCart.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SarvamCart.Controllers
{
    public class QuotationHistoryController : Controller
    {
        private readonly IQuotationService QuotationService;
        private readonly IQuotationDetailService QuotationDetailService;
        private readonly IProductImageService ProductImageService;
        private readonly Models.UserManager UserManager;

        public QuotationHistoryController(IQuotationService _quotationServices, UserService _userservice, IQuotationDetailService _quotationDetailService, IProductImageService _ProductImageService, Models.UserManager _userManager)
        {
            QuotationService = _quotationServices;
            UserManager = _userManager;
            ProductImageService = _ProductImageService;
            QuotationDetailService = _quotationDetailService;
        }
        // GET: OrderHistory
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult QuotationHistory(int? userId)
        {
            userId = UserManager.UserDetail.UserID;
            ViewBag.Quotations = QuotationService.FindBy(x => x.UserId == userId && (x.StatusOfOrder==1 || x.StatusOfOrder == 2)).ToList();
            ViewBag.QuotationsCount = QuotationService.FindBy(x => x.UserId == userId && (x.StatusOfOrder==1 || x.StatusOfOrder == 2)).Count();

            if (userId>0)
            {
                ViewModelQuotaion viewModel = new ViewModelQuotaion();
                
            }

            return View();
        }
        //public ActionResult QuotationDetails(int? id)
        //{
        //    var quotation = QuotationService.FirstOrDefault(x => x.Id == id);

        //    ViewModelQuotaion viewModel = new ViewModelQuotaion();
        //    if (id > 0)
        //    {
        //        viewModel.UserName = quotation.User.UserName;
        //        viewModel.UserEmail = quotation.User.EmailAddress;
        //        viewModel.ContactNumber = quotation.User.ContactNumber;
        //        viewModel.CreatedOn = (DateTime)quotation.CreatedOn;
        //        viewModel.GrantTotal = quotation.GrantTotal ??0;

        //        viewModel.CartDetails = quotation.QuotationDetails.Select(x => new ManageQuotaionDetailModel
        //        {
        //            Id = x.Id,
        //            QuotationId = x.QuotationId ?? 0,
        //            ProductId = x.ProductId ?? 0,
        //            ProductName = x.Product.Title,
        //            ProductCode = x.Product.ProductCode,
        //            FinalPrice = x.FinalPrice ?? 0,
        //            Quentity = Convert.ToInt32(x.Quentity ?? 0),
        //            ProductImage = ProductImageService.FirstOrDefault(p => p.ProductId == x.ProductId && !p.IsDeleted).InnerImages
        //        }).ToList();
        //    }
        //    return View(viewModel);
        //}

        public ActionResult QuotationDetails(int? id)
        {
           
            var quotation = QuotationService.FirstOrDefault(x => x.Id == id);

            ViewModelQuotaion viewModel = new ViewModelQuotaion();
            if (id > 0)
            {
                viewModel.UserName = quotation.User.UserName;
                viewModel.UserEmail = quotation.User.EmailAddress;
                viewModel.ContactNumber = quotation.User.ContactNumber;
                viewModel.CreatedOn = (DateTime)quotation.CreatedOn;
                viewModel.GrantTotal = quotation.GrantTotal ?? 0;
                viewModel.Id = quotation.Id;

                viewModel.CartDetails = quotation.QuotationDetails.Select(x => new ManageQuotaionDetailModel
                {
                    Id = x.Id,
                    QuotationId = x.QuotationId ?? 0,
                    ProductId = x.ProductId ?? 0,
                    ProductName = x.Product.Title,
                    ProductCode = x.Product.ProductCode,
                    FinalPrice = x.FinalPrice ?? 0,
                    Quentity = Convert.ToInt32(x.Quentity ?? 0),
                    ProductImage = ProductImageService.FirstOrDefault(p => p.ProductId == x.ProductId && !p.IsDeleted).InnerImages
                }).ToList();
            }
            return View(viewModel);
        }

        public ActionResult _QuotationPdf(int? id)
        {
            var quotation = QuotationService.FirstOrDefault(x => x.Id == id);

            ViewModelQuotaion viewModel = new ViewModelQuotaion();
            if (id > 0)
            {
                viewModel.UserName = quotation.User.UserName;
                viewModel.UserEmail = quotation.User.EmailAddress;
                viewModel.ContactNumber = quotation.User.ContactNumber;
                viewModel.CreatedOn = (DateTime)quotation.CreatedOn;
                viewModel.GrantTotal = quotation.GrantTotal ?? 0;

                viewModel.CartDetails = quotation.QuotationDetails.Select(x => new ManageQuotaionDetailModel
                {
                    Id = x.Id,
                    QuotationId = x.QuotationId ?? 0,
                    ProductId = x.ProductId ?? 0,
                    ProductName = x.Product.Title,
                    ProductCode = x.Product.ProductCode,
                    FinalPrice = x.FinalPrice ?? 0,
                    Quentity = Convert.ToInt32(x.Quentity ?? 0),
                    ProductImage = ProductImageService.FirstOrDefault(p => p.ProductId == x.ProductId && !p.IsDeleted).InnerImages
                }).ToList();
            }

            return PartialView(viewModel);
        }

        public ActionResult DownloadPdf(int? id)
        {
            return new ActionAsPdf("_QuotationPdf",new { id=id}) { FileName = "Quotation.pdf" , PageSize = Size.A4 };
            //return new ActionAsImage("_Test"){ FileName = "test.pdf"  };
        }

    }
}