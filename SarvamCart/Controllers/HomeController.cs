﻿using SarvamCart.Domain.Data;
using SarvamCart.Domain.Models;
using SarvamCart.Domain.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SarvamCart.Controllers
{
    public class HomeController : Controller
    {
        private SarvamEntities db = new SarvamEntities();
        private readonly IProductService ManageProductServices;
        private readonly ICategoryService ManageCategoryServices;
        private readonly IContactUsService ContactUsServices;
        private readonly IQuotationService QuotationService;
        private readonly IMailService MailServices;

        public HomeController(IProductService _manageProductService, IQuotationService _quotationServices, ICategoryService _manageCategoryServices, IContactUsService _contactUsServices, IMailService _mailServices)
        {
            ManageProductServices = _manageProductService;
            ManageCategoryServices = _manageCategoryServices;
            ContactUsServices = _contactUsServices;
            QuotationService = _quotationServices;
            MailServices = _mailServices;
        }
        public ActionResult Index()
        {
            var result = new List<Product>();
            result = ManageProductServices.FindBy(x => x.IsDeleted == false && x.DisplayHome == true).OrderByDescending(o => o.Id).Take(8).ToList();
            if (result.Count() == 0)
            {
                ViewBag.Message = "No Products";
            }
            List<ViewModelProduct> model = new List<ViewModelProduct>();

            foreach (var item in result)
            {
                model.Add(new ViewModelProduct
                {
                    Description = item.Description,
                    Id = item.Id,
                    Title = item.Title,
                    CategoryTypeId = item.CategoryTypeId,
                    IsActive = item.IsActive,
                    ProductCode = item.ProductCode,
                    Stock = item.Stock,
                    DisplayHome = item.DisplayHome,
                    InnerImage = db.ProductImages.Where(x => x.ProductId == item.Id && x.IsDeleted == false).ToList()
                });
            }
            return View(model);
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Contact(ContactUsModel viewModel)
        {

            ContactUsServices.Save(viewModel);

            //notify to admin about contact us via email

            var body = viewModel.Message;
            var to = "";
            var from = "";
            var subject = "";
            var html = "";
            to = ConfigurationManager.AppSettings["AdminMailID"];
            from = viewModel.Email;
            subject = "Contact Message";
            html = "<div style='border:1px solid ; width:80%;'><div class='leave' style='margin:0 10% 0 10%;'><p>Name:" + viewModel.Name + "<br>" + "Contact Number:" + viewModel.ContactNumber +
                "<br>" + "Date:" + DateTime.Now +
                      "<p> " + body + "</p>";
            // +"</p><br> <br> " + viewModel.Name + " <br><br></div></div>";
            MailServices.Send(from, to, subject, html);
            if (viewModel != null)
            {
                TempData["Msg"] = "your resquest is send successfully";

            }
            ModelState.Clear();
            return View();
        }

        public ActionResult Product(int id)
        {
            //ManageProduct viewModel = new ManageProduct();
            //var result = new List<Product>();

            //result = ManageProductServices.FindBy(x => x.IsDeleted == false && x.IsActive == true && x.CategoryTypeId == id).OrderByDescending(o => o.Id).Skip(0).Take(6).ToList();
            //if (result.Count() == 0)
            //{
            //    ViewBag.Message = "No Products";
            //}
            //List<ViewModelProduct> model = new List<ViewModelProduct>();

            //foreach (var item in result)
            //{
            //    model.Add(new ViewModelProduct
            //    {
            //        Description = item.Description,
            //        Id = item.Id,
            //        Title = item.Title,
            //        InnerImage = db.ProductImages.Where(x => x.ProductId == item.Id && x.IsDeleted == false).ToList()
            //    });
            //}
            //ViewBag.categoryName = ManageCategoryServices.FindBy(x => x.Id == id).ToList()[0].CategoryName;
            if (id != 0)
            {
                ViewBag.categoryName = ManageCategoryServices.FindBy(x => x.Id == id).FirstOrDefault().CategoryName;
            }
            else
            {
                ViewBag.categoryName = "All";
            }

            //var category = new List<CategoryType>();
            ViewBag.category = ManageCategoryServices.FindBy(x => x.IsActive == true).Where(x => x.IsDeleted == false).ToList();
            ViewBag.CategoryId = id;


            return View();
        }
        public ActionResult _Categories()
        {
            var category = new List<CategoryType>();
            ViewBag.category = ManageCategoryServices.FindBy(x => x.IsActive == true).Where(x => x.IsDeleted == false).OrderBy(o => o.DisplayOrder).ToList();
            return PartialView();
        }

        public ActionResult _Products(int currentPageNo, int categoryId)
        {
            ManageProduct viewModel = new ManageProduct();

            int pageSize = 8;
            int skipRows = currentPageNo * pageSize;
            var products = ManageProductServices.FindBy(x => x.IsDeleted == false && x.IsActive == true && (categoryId == 0 || x.CategoryTypeId == categoryId));
            var totalProducts = products.Count();

            var result = products.OrderByDescending(o => o.Id).Skip(skipRows).Take(pageSize).Select(x =>
            new ViewModelProduct
            {
                Description = x.Description,
                Id = x.Id,
                Title = x.Title,
                InnerImage = x.ProductImages.Where(i => i.IsDeleted == false).ToList()
            }).ToList();

            var totalProductDisplayed = (currentPageNo + 1) * pageSize;
            var hasMoreProducts = totalProductDisplayed < totalProducts;
            ViewBag.HasMoreRows = hasMoreProducts ? "true" : "false";

            return PartialView(result);
        }
    }
}