﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SarvamCart.Controllers
{
    public class BaseController : Controller
    {
        public System.Web.SessionState.HttpSessionState cartCounter
        {
            get
            {
                return System.Web.HttpContext.Current.Session;
            }
        }
    }
}