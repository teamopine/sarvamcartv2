﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using SarvamCart.Domain.Data;
using SarvamCart.Domain.Models;
using SarvamCart.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using TeamOpine.Lib.Security;

namespace SarvamCart.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IUserService UserService;
        private readonly Models.UserManager UserManager;
        private readonly IQuotationService QuotationService;
        private readonly IQuotationDetailService QuotationDetailService;

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public AccountController(UserService _UserService, IQuotationDetailService _quotationDetailService, IQuotationService _quotationServices, Models.UserManager _UserManager)
        {
            QuotationService = _quotationServices;
            UserService = _UserService;
            UserManager = _UserManager;
            QuotationDetailService = _quotationDetailService;
        }


        // GET: Account
        public ActionResult Login(string returnUrl = "")
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginModel viewModel, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = UserService.FirstOrDefault(x => x.EmailAddress == viewModel.EmailAddress);
                if (user != null && PasswordHash.ValidatePassword(viewModel.Password, user.Passowrd))
                {
                    if (user.EmailComfirm == true)
                    {
                        //Set identity here.
                        var identity = UserManager.CreateUserIdentity(user);
                        if (identity.Claims.Any(x => x.Type == ClaimTypes.NameIdentifier))
                        {
                            var principal = new ClaimsPrincipal(new[] { identity });
                            AuthenticationManager.SignIn(new AuthenticationProperties()
                            {
                                IsPersistent = false
                            }, identity);
                            if (!string.IsNullOrEmpty(returnUrl))
                                return Redirect(returnUrl);
                            else if (user.UserRoles.ToLower().Contains("admin"))
                                return RedirectToAction("Index", "DashBoard", new { Area = "Admin" });
                            else if (user.UserRoles.ToLower().Contains("user"))
                            {
                                this.cartCounter["SessionCartCounter"] = QuotationService.FirstOrDefault(x => x.UserId == user.Id && x.StatusOfOrder == 0).QuotationDetails.Count();
                                return RedirectToAction("Index", "Home", new { Area = "" });
                            }
                            else
                                return RedirectToAction("Index", "Home");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Error", "Your email is not confirm yet!!");
                    }
                }
                else
                {
                    ModelState.AddModelError("Error", "Invalid username or password");
                }

                //user.Passowrd = PasswordHash.CreateHash(user.Passowrd);
                //ModelState.Clear();
            }
            return View();
        }

        public ActionResult Registration()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Registration(RegistrationModel viewModel)
        {
            var existEmail = UserService.FindBy(x => x.EmailAddress == viewModel.EmailAddress).ToList();

            if (existEmail.Count() == 0)
            {
                UserService.Save(viewModel);
                TempData["SuccessMsg"] = "Please ckeck your email for confirmation";
            }
            else
            {
                TempData["ErrorMsg"] = "This Email is Already Exist";
                return View(viewModel);
            }
            ModelState.Clear();
            return View();
        }

        public ActionResult ConfirmEmail(string token, string email, DateTime dtt = default(DateTime))
        {
            var dtTill = dtt.AddHours(12);
            if (dtTill > DateTime.Now && token != null && email != null)
            {
                var data = UserService.FirstOrDefault(x => x.EmailAddress == email);

                if (data != null)
                {
                    data.EmailComfirm = true;
                    UserService.Edit(data);
                    UserService.Save();
                }
            }
            else
            {
                TempData["Error"] = "Oops!! Link is Active for 12 hours only!";
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult ForgetPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ForgetPassword(RegistrationModel model)
        {

            var user = UserService.ForgetPassword(model.EmailAddress);
            if (user == true)
            {
                TempData["Success"] = "Checkout your mail";
                //return RedirectToAction("Login", "Account");
            }
            else
            {
                TempData["ErrorMsg"] = "Oops Wrong Email!! ";
                // return RedirectToAction("UserLogin", "Account");
            }
            return View();
        }

        public ActionResult ResetPassword(int id, string email, DateTime dtt = default(DateTime))
        {
            ResetPasswordModel viewModel = new ResetPasswordModel();

            var dtTill = dtt.AddDays(1);
            if (dtTill > DateTime.Now)
            {
                viewModel.Id = id;
                viewModel.EmailAddress = email;
            }
            else
            {
                TempData["error"] = "Link was active for 24 hours only";
                return RedirectToAction("UserLogin", "Account");
            }
            return View();
        }
        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordModel viewModel)
        {
            UserService.ResetPassword(viewModel);
            TempData["Success"] = "Password Changed Successfully";
            return View();
        }
        public ActionResult LogOut()
        {
            Session.RemoveAll();
            Session.Abandon();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account", new { area = "" });
        }

        #region --Helper --


        #endregion
    }
}