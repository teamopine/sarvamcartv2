﻿using SarvamCart.Domain.Data;
using SarvamCart.Domain.Models;
using SarvamCart.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SarvamCart.Controllers
{
    public class ProductDetailController : Controller
    {
        private readonly IProductService ProductService;
        //private readonly IProductImageService ManageProductImageServices;
        public ProductDetailController(IProductService _manageProductServices )
        {
            ProductService = _manageProductServices;
        }

        // GET: Product
        public ActionResult Index(int Id)
        {
            ManageProduct viewModel = new ManageProduct();

            var result = ProductService.FirstOrDefault(x => x.Id == Id && x.IsDeleted == false);
            if (result == null)
            {
                ViewBag.Message = "No Product";
            }
            else
            {
                viewModel.Id = result.Id;
                viewModel.Title = result.Title;
                viewModel.Stock = result.Stock;
                //viewModel.Image = result.Image;
                viewModel.IsActive = result.IsActive;
                viewModel.DisplayHome = result.DisplayHome;
                viewModel.Description = result.Description;
                viewModel.CategoryTypeId = result.CategoryTypeId;
                viewModel.Price = result.Price;
                viewModel.ProductCode = result.ProductCode;

                viewModel.CategoryType = result.CategoryType.CategoryName;

                viewModel.InnerImage=result.ProductImages?.Where(x=>x.IsDeleted==false).Select(x => new ManageProductImages { Id = x.Id, ProductId = x.ProductId, ImageName = x.InnerImages }).Take(5).ToList();
                viewModel.RelatedProduct = ProductService.FindBy(x => x.Id != result.Id && x.CategoryTypeId == result.CategoryTypeId && x.IsActive && x.IsDeleted==false).Take(12).ToList();
            }

            return View(viewModel);
        }
    }
}