﻿using SarvamCart.Domain.Data;
using SarvamCart.Domain.Models;
using SarvamCart.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SarvamCart.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class CategoryController : Controller
    {
        //private SarvamEntities db = new SarvamEntities();
        private readonly ICategoryService CategoryService;
        private readonly IProductService ProductService;

        public CategoryController(ICategoryService _categoryServices, ProductService _productServices)
        {
            CategoryService = _categoryServices;
            ProductService = _productServices;

        }

        // GET: Admin/Category
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetCategory()
        {

            ViewModelCategory viewModel = new ViewModelCategory();

            var result = CategoryService.FindBy(x => x.IsDeleted == false).Select(x => new ViewModelCategory
            {
                Id = x.Id,
                CategoryName = x.CategoryName,
                DisplayOrder = x.DisplayOrder ?? 0,
                IsActive=x.IsActive

            }).ToList();
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Manage(int? Id)
        {

            ManageCategory viewModel = new ManageCategory();
            if (Id > 0)
            {
                var result = CategoryService.FirstOrDefault(x => x.Id == Id);

                viewModel.Id = result.Id;
                viewModel.CategoryName = result.CategoryName;
                viewModel.DisplayOrder = result.DisplayOrder ?? 0;
                viewModel.IsActive = result.IsActive;
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult Manage(ManageCategory viewModel)
        {

            if (viewModel.Id == null)
            {
                var existname = CategoryService.FindBy(x => x.CategoryName == viewModel.CategoryName && x.IsDeleted != true).Any();
                if (existname == true)
                {
                    TempData["Error"] = "This category name is alraedy exist";
                }
                else
                {
                    CategoryService.Save(viewModel);
                    return RedirectToAction("Index");
                }
            }

            else
            {
                if (ModelState.IsValid)
                {
                    CategoryService.Save(viewModel);
                    return RedirectToAction("Index");
                }
            }

            return View(viewModel);
        }
        [HttpPost]
        public JsonResult Delete(int Id)
        {
            var product = ProductService.FindBy(x => x.CategoryTypeId == Id && x.IsDeleted == false).ToList();
            if (product.Count() > 0)
            {
                return Json(new { IsSuccess = false });
            }
            else
            {
                CategoryService.Delete(Id);
            }
            return Json(new { IsSuccess = true });
        }

        [HttpPost]
        public ActionResult UpdateIsActive(int Id, bool IsActive)
        {
            //var data = CategoryService.FindBy(x => x.Id == Id).FirstOrDefault();

            var data = CategoryService.FindBy(x => x.Id == Id).FirstOrDefault();
            if (data != null)
            {
                data.IsActive = IsActive;
                CategoryService.Edit(data);
                CategoryService.Save();
            }


            //CategoryType category = new CategoryType();
            //if (Id>0)
            //{

            //    category.CategoryName = data.CategoryName;
            //    category.DisplayOrder = data.DisplayOrder;
            //    category.Id = Id;
            //   // category.IsActive = Convert.ToBoolean(IsActive);
            //    category.IsActive = IsActive;
            //}

            // db.Entry(category).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();

            return Json(new { IsSuccess = true });
        }

    }
}