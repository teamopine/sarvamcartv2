﻿using SarvamCart.Domain.Data;
using SarvamCart.Domain.Models;
using SarvamCart.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SarvamCart.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class ContactUsUsersController : Controller
    {
        private readonly IContactUsService ContactUsService;

        public ContactUsUsersController(IContactUsService _contactUsService)
        {
            ContactUsService = _contactUsService;
        }

        // GET: Admin/ContactUsUsers
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Users()
        {
            ContactUsModel model = new ContactUsModel();
            var result = ContactUsService.FindBy(x=>x.Name!=null).ToList();


            //SarvamEntities _dbContext = new SarvamEntities();
            //_dbContext.Configuration.ProxyCreationEnabled = false;
            //var result = new List<ContactU>();
            //result = _dbContext.ContactUs.OrderByDescending(x => x.Id).ToList();



            //var result = new List<Product>();
            //result = ManageProductServices.FindBy(x => x.IsDeleted == false).ToList();
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }


    }
}