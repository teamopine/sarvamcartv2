﻿using SarvamCart.Domain.Models;
using SarvamCart.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SarvamCart.Areas.Admin.Controllers
{
    [Authorize(Roles ="admin")]
    public class DashBoardController : Controller
    {
        private readonly IProductService ProductServices;
        private readonly IUserService UserServices;

        public DashBoardController(IProductService _productServices,IUserService _userServices)
        {
            ProductServices = _productServices;
            UserServices = _userServices;
        }

        // GET: Admin/DashBoard
        public ActionResult Index()
        {
            ManageProduct viewModel = new ManageProduct();
            TempData["totalProduct"] = ProductServices.FindBy(x=>x.IsDeleted == false).Count();
            TempData["activeProduct"] = ProductServices.FindBy(x => x.IsActive == true && !x.IsDeleted).Count();
            TempData["totalUser"] = UserServices.FindBy(x => x.IsActive == true && !x.IsDeleted).Count();

            return View();
        }
    }
}