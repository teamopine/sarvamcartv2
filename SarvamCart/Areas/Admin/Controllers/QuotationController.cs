﻿using Rotativa;
using SarvamCart.Domain.Models;
using SarvamCart.Domain.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SarvamCart.Areas.Admin.Controllers
{
    //[Authorize(Roles = "admin")]
    public class QuotationController : Controller
    {
        private readonly IQuotationService QuotationService;
        private readonly IQuotationDetailService QuotationDetailService;
        private readonly IProductImageService ProductImageService;
        private readonly IUserService UserService;
        private readonly IMailService MailService;
        // GET: Admin/Quotation

        public QuotationController(IQuotationService _quotationServices, IProductImageService _productImageService, IQuotationDetailService _quotationDetailService,IUserService _userService,IMailService _mailService)
        {
            QuotationService = _quotationServices;
            ProductImageService = _productImageService;
            QuotationDetailService = _quotationDetailService;
            UserService = _userService;
            MailService = _mailService;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult QuotationList()
        {
            var result = QuotationService.FindBy(x => x.StatusOfOrder == 1).Select(x => new ViewModelQuotaion
            {
                Id = x.Id,
                UserId = x.UserId ?? 0,
                UserName = x.User.UserName,
                CreatedOn = (DateTime)x.CreatedOn,
                StatusOfOrder = x.StatusOfOrder ?? 0,
            }).OrderByDescending(x => x.Id).ToList();
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult QuotationDetails(int? id)
        {
            var quotation = QuotationService.FirstOrDefault(x => x.Id == id);

            ViewModelQuotaion viewModel = new ViewModelQuotaion();
            if (id > 0)
            {
                viewModel.UserName = quotation.User.UserName;
                viewModel.UserEmail = quotation.User.EmailAddress;
                viewModel.ContactNumber = quotation.User.ContactNumber;
                viewModel.CreatedOn = (DateTime)quotation.CreatedOn;
                viewModel.Id = quotation.Id;

                viewModel.CartDetails = quotation.QuotationDetails.Select(x => new ManageQuotaionDetailModel
                {
                    Id = x.Id,
                    QuotationId = x.QuotationId ?? 0,
                    ProductId = x.ProductId ?? 0,
                    ProductName = x.Product.Title,
                    ProductCode = x.Product.ProductCode,
                    FinalPrice = x.FinalPrice ?? 0,
                    Quentity = Convert.ToInt32(x.Quentity ?? 0),
                    ProductImage = ProductImageService.FirstOrDefault(p => p.ProductId == x.ProductId && !p.IsDeleted).InnerImages
                }).ToList();
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult QuotationDetails(ViewModelQuotaion viewModel, decimal? grandTotal)
        {
            var quotation = QuotationService.FirstOrDefault(x => x.Id == viewModel.Id);
            if (quotation != null)
            {
                quotation.GrantTotal = grandTotal;
                quotation.StatusOfOrder = 2;

                QuotationService.Edit(quotation);
                QuotationService.Save();
            }

            foreach (var data in viewModel.CartDetails)
            {
                var result = QuotationDetailService.FirstOrDefault(x => x.Id == data.Id);
                result.FinalPrice = data.FinalPrice;
                QuotationDetailService.Edit(result);
                QuotationDetailService.Save();
            }
            return View(viewModel);
        }

        public ActionResult DownloadQuotation(int? id)
        {
            var quotation = QuotationService.FirstOrDefault(x => x.Id == id);

            ViewModelQuotaion viewModel = new ViewModelQuotaion();
            if (id > 0)
            {
                viewModel.UserName = quotation.User.UserName;
                viewModel.UserEmail = quotation.User.EmailAddress;
                viewModel.ContactNumber = quotation.User.ContactNumber;
                viewModel.CreatedOn = (DateTime)quotation.CreatedOn;
                viewModel.GrantTotal = quotation.GrantTotal ?? 0;
                viewModel.Id = quotation.Id;

                viewModel.CartDetails = quotation.QuotationDetails.Select(x => new ManageQuotaionDetailModel
                {
                    Id = x.Id,
                    QuotationId = x.QuotationId ?? 0,
                    ProductId = x.ProductId ?? 0,
                    ProductName = x.Product.Title,
                    ProductCode = x.Product.ProductCode,
                    FinalPrice = x.FinalPrice ?? 0,
                    Quentity = Convert.ToInt32(x.Quentity ?? 0),
                    ProductImage = ProductImageService.FirstOrDefault(p => p.ProductId == x.ProductId && !p.IsDeleted).InnerImages
                }).ToList();
            }
            return View(viewModel);
        }

        public ActionResult _Quotationpdf(int? id, decimal? grandTotal)
        {
            var quotation = QuotationService.FirstOrDefault(x => x.Id == id);
            ViewModelQuotaion viewModel = new ViewModelQuotaion();
            viewModel.UserName = quotation.User.UserName;
            viewModel.UserEmail = quotation.User.EmailAddress;
            viewModel.ContactNumber = quotation.User.ContactNumber;
            viewModel.CreatedOn = (DateTime)quotation.CreatedOn;
            viewModel.GrantTotal = quotation.GrantTotal ?? 0;

            viewModel.CartDetails = quotation.QuotationDetails.Select(x => new ManageQuotaionDetailModel
            {
                Id = x.Id,
                QuotationId = x.QuotationId ?? 0,
                ProductId = x.ProductId ?? 0,
                ProductName = x.Product.Title,
                ProductCode = x.Product.ProductCode,
                FinalPrice = x.FinalPrice ?? 0,
                Quentity = Convert.ToInt32(x.Quentity ?? 0),
                ProductImage = ProductImageService.FirstOrDefault(p => p.ProductId == x.ProductId && !p.IsDeleted).InnerImages
            }).ToList();

            return PartialView(viewModel);
        }

        // made quotation pdf  
        public ActionResult DownloadPdf(int? id)
        {
            var quotation = QuotationService.FirstOrDefault(x => x.Id == id);

            return new ActionAsPdf("_Quotationpdf", new { id = id }) { FileName = "Quotation.pdf" };
            {
                var body = "Your Quotation";
                var to = "";
                var from = "";
                var subject = "";
                var html = "";
                to = ConfigurationManager.AppSettings["AdminMailID"];
                from = quotation.User.EmailAddress;
                subject = "Contact Message";
                html = "<div style='border:1px solid ; width:80%;'><div class='leave' style='margin:0 10% 0 10%;'><p>Name:" + quotation.User.UserName + "<br>" + "Contact Number:" + quotation.User.ContactNumber +
                    "<br>" + "Date:" + DateTime.Now +
                          "<p> " + body + "</p>";
                MailService.Send(from, to, subject, html);
            };


        }

    }
}