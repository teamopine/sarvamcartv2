﻿using Newtonsoft.Json;
using SarvamCart.Custom;
using SarvamCart.Domain.Data;
using SarvamCart.Domain.Models;
using SarvamCart.Domain.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SarvamCart.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class ProductController : Controller
    {
        //private SarvamEntities db = new SarvamEntities();
        private readonly IProductService ProductServices;
        private readonly IProductImageService ProductImageService;
        private readonly ICategoryService CategoryService;

        public ProductController(IProductService _productServices, ProductImageService _ProductImageService, CategoryService _CategoryService)
        {
            ProductServices = _productServices;
            ProductImageService = _ProductImageService;
            CategoryService = _CategoryService;
        }

        // GET: Admin/Product
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetProducts()
        {

            // ViewModelProduct viewModel = new ViewModelProduct();

            var result = ProductServices.FindBy(x => x.IsDeleted == false).Select(x => new ViewModelProduct
            {
                Id = x.Id,
                Title = x.Title,
                ProductCode = x.ProductCode,
                Stock = x.Stock,
                IsActive = x.IsActive,
                DisplayHome = x.DisplayHome,
                Image = x.ProductImages.Where(o => o.IsDeleted == false).FirstOrDefault().InnerImages,
                Category = x.CategoryType.CategoryName,

            }).OrderByDescending(x => x.Id).ToList();


            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Manage(int? Id)
        {
            ManageProduct viewModel = new ManageProduct();
            // viewModel.CategoryTypes = CategoryService.FindBy(x => x.IsDeleted == false && x.IsActive == true).Select(x => new SelectListItem { Text = x.CategoryName, Value = x.Id.ToString() });
            viewModel.CategoryTypes = CategoryService.FindBy(x => x.IsDeleted == false && x.IsActive == true).OrderBy(x => x.DisplayOrder).Select(x => new SelectListItem { Text = x.Id.ToString(), Value = x.CategoryName }).ToList();
            if (Id > 0)
            {
                var result = ProductServices.FirstOrDefault(x => x.Id == Id);
                if (result != null)
                {
                    viewModel.Id = result.Id;
                    viewModel.Title = result.Title;
                    viewModel.Stock = result.Stock;
                    //viewModel.Image = result.Image;
                    viewModel.IsActive = result.IsActive;
                    viewModel.DisplayHome = result.DisplayHome;
                    viewModel.Description = result.Description;
                    viewModel.CategoryTypeId = result.CategoryTypeId;
                    viewModel.Price = result.Price;
                    viewModel.ProductCode = result.ProductCode;
                    viewModel.InnerImage = result.ProductImages?.Where(x => x.IsDeleted == false).Select(x => new ManageProductImages { Id = x.Id, ProductId = x.ProductId, ImageName = x.InnerImages }).ToList();
                }
            }
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult Manage(ManageProduct viewModel, HttpPostedFileBase[] ProductImagespath)
        {
            viewModel.CategoryTypes = CategoryService.FindBy(x => x.IsDeleted == false && x.IsActive == true).OrderBy(x => x.DisplayOrder).Select(x => new SelectListItem { Text = x.CategoryName, Value = x.Id.ToString() });
            string FileName = string.Empty;
            //if (Image != null)
            //{
            //    var newFileName = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(Image.FileName);
            //    string path = Path.Combine(Server.MapPath("~/Assets/UploadedFiles"), newFileName);
            //    Image.SaveAs(path);
            //    FileName = Image.FileName;
            //    viewModel.Image = newFileName;
            //    ViewBag.FileStatus = "File uploaded successfully.";

            //    if (viewModel.Id > 0 && viewModel.Image == null)
            //    {
            //        viewModel.Image = viewModel.Image;
            //    }
            //}
            //else
            //{
            //    ViewBag.FileStatus = "Error while file uploading.";
            //}

            if (ModelState.IsValid)
            {
                var productID = ProductServices.Save(viewModel);
                if (productID > 0)
                {
                    if (ProductImagespath != null)
                    {
                        foreach (var item in ProductImagespath)
                        {
                            var newFileName = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(item.FileName);
                            FileName = Path.GetExtension(item.FileName);
                            FileName = Path.GetFileName(item.FileName);
                            string path = Server.MapPath("~/Assets/UploadedFiles");
                            string ImagePath = Path.Combine(path, newFileName);
                            item.SaveAs(ImagePath);
                            ProductImageService.Add(new ProductImage { ProductId = productID, InnerImages = newFileName });
                        }
                        ProductImageService.Save();
                    }
                }
                return Json(new { result = "Redirect", url = Url.Action("Index", "Product") });
            }
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Delete(int Id)
        {
            ProductServices.Delete(Id);
            return Json(new { IsSuccess = true });
        }
       
        [HttpPost]
        public ActionResult DeleteImage(int Id)
        {
            ProductImageService.Delete(Id);
            return Json(new { Success = true });
        }
        
        [HttpPost]
        public ActionResult UpdateIsActive(int Id, bool IsActive)
        {
            var data = ProductServices.FirstOrDefault(x => x.Id == Id);
            if (data != null)
            {
                data.IsActive = IsActive;
                if (IsActive == false)
                {
                    data.DisplayHome = false;
                }

                ProductServices.Edit(data);
                ProductServices.Save();
            }
            return Json(new { IsSuccess = true });
        }
    }
}