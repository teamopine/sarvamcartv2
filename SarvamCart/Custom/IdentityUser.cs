﻿using SarvamCart.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SarvamCart.Custom
{
    public static  class IdentityUser
    {
        public static UserManager UserManager => DependencyResolver.Current.GetService<UserManager>();
    }
}