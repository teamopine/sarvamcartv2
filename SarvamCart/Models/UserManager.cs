﻿using SarvamCart.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace SarvamCart.Models
{
    public class UserManager
    {
        public UserDetail UserDetail
        {
            get
            {
                var _details = new UserDetail();
                var claims = (HttpContext.Current.User.Identity as ClaimsIdentity).Claims;
                if (claims != null && claims.Any())
                {
                    try
                    {
                        _details.UserID = Convert.ToInt32(claims.FirstOrDefault(o => o.Type == "UserID")?.Value);
                        if (claims.Any(o => o.Type == ClaimTypes.GivenName))
                            _details.Name =  claims.First(o => o.Type == ClaimTypes.GivenName).Value;
                            _details.Role = claims.FirstOrDefault(o => o.Type == "Role")?.Value ?? string.Empty;
                    }
                    catch (Exception ex)
                    {
                    }
                }
                return _details;
            }
        }
        public ClaimsIdentity CreateUserIdentity(User model)
        {
            var identity = new ClaimsIdentity("ApplicationCookie");
            identity.AddClaim(new Claim("UserID", model.Id.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.Name, model.UserName));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, model.Id.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.Role, model.UserRoles));
            return identity;
        }
    }
    public class UserDetail
    {
        public int? UserID { get; set; }
        public string Name { get; set; }
        public string NameIdentifier { get; set; }
        public string Role { get; set; }
    }
}