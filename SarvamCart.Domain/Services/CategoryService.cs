﻿using SarvamCart.Domain.Data;
using SarvamCart.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SarvamCart.Domain.Services
{
    public interface ICategoryService : ISarvamCartServiceBase<CategoryType>
    {
        int Save(ManageCategory model);
        void Delete(int id);
    }
    public class CategoryService : SarvamCartServiceBase<CategoryType>, ICategoryService
    {
        public SarvamCartServiceBase<CategoryType> manageCategoryServices;

        public CategoryService(SarvamEntities context, SarvamCartServiceBase<CategoryType> _manageCategoryServices) : base(context)
        {
            manageCategoryServices = _manageCategoryServices;
        }

        public int Save(ManageCategory model)
        {
            CategoryType categoryModel = new CategoryType();
            if (model.Id > 0)
            {
                categoryModel.Id = model.Id ?? 0;
                categoryModel.CategoryName = model.CategoryName;
                categoryModel.IsActive = model.IsActive;
                categoryModel.DisplayOrder = model.DisplayOrder;

                Edit(categoryModel);
                Save();
            }
            else
            {
                categoryModel.CategoryName = model.CategoryName;
                categoryModel.IsActive = model.IsActive;
                categoryModel.DisplayOrder = model.DisplayOrder;

                Add(categoryModel);
                Save();

            }

            return categoryModel.Id;
        }

        public void Delete(int id)
        {
            var category = FirstOrDefault(x => x.Id == id);

            category.IsDeleted = true;
            Edit(category);
            Save();
        }
    }
}
