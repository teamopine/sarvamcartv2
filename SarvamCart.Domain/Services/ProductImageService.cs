﻿using SarvamCart.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SarvamCart.Domain.Services
{
    public interface IProductImageService : ISarvamCartServiceBase<ProductImage>
    {
        int Save(ManageProductImages model);
        void Delete(int id);

    }
    public class ProductImageService : SarvamCartServiceBase<ProductImage>, IProductImageService
    {
        public SarvamCartServiceBase<Product> manageProductImageServices;

        public ProductImageService(SarvamEntities context, SarvamCartServiceBase<Product> _manageProductImageServices) : base(context)
        {
            manageProductImageServices = _manageProductImageServices;
        }


        public int Save(ManageProductImages model)
        {
            ProductImage productImage = new ProductImage();

            productImage.ProductId = model.ProductId;
            productImage.InnerImages = model.ImageName;

            Add(productImage);
            Save();
            return productImage.Id;

        }

       

        public void Delete(int id)
        {
            var productImages = FirstOrDefault(x => x.Id == id);

            productImages.IsDeleted = true;
            Edit(productImages);
            Save();
        }

    }

}
