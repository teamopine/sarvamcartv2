﻿using SarvamCart.Domain.Data;
using SarvamCart.Domain.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TeamOpine.Lib.Security;

namespace SarvamCart.Domain.Services
{
    public interface IUserService : ISarvamCartServiceBase<User>
    {
        int Save(RegistrationModel model);
        bool ForgetPassword(string Email);
        int ResetPassword(ResetPasswordModel model);
    }
    public class UserService : SarvamCartServiceBase<User>, IUserService
    {
        public SarvamCartServiceBase<Product> manageProductServices;
        private readonly IMailService MailService;
        public UserService(SarvamEntities context, MailService _mailservice) : base(context)
        {
            MailService = _mailservice;
        }

        public int Save(RegistrationModel model)
        {
            User userModel = new User();

            userModel.UserName = model.UserName;
            userModel.ContactNumber = model.ContactNumber;
            userModel.EmailAddress = model.EmailAddress;
            userModel.Passowrd = PasswordHash.CreateHash(model.Password);
            userModel.Token = Guid.NewGuid();
            userModel.UserRoles = "user";
            userModel.CreatedOn = DateTime.Now;
            userModel.UpdatedOn = DateTime.Now;
            userModel.IsActive = true;

            Add(userModel);
            Save();

            ConfirmMail(userModel);


            return userModel.Id;
        }

        public bool ForgetPassword(string Email)
        {
            var user = FirstOrDefault(x => x.EmailAddress == Email);
            if (user != null)
            {
                SendForgetPasswordEmail(user);
                Save();
                return true;
            }
            else
            {
                return false;
            }

        }

        public int ResetPassword(ResetPasswordModel model)
        {
            var user = FirstOrDefault(x => x.Id == model.Id);
            if (user != null)
            {
                user.Passowrd = PasswordHash.CreateHash(model.Password);
                Edit(user);
                Save();

            }
            return user.Id;
        }


        public void ConfirmMail(User user)
        {
            var from = "";
            var to = "";
            var subject = "";
            var html = "";
            from = ConfigurationManager.AppSettings["AdminMailID"];
            to = user.EmailAddress;
            subject = "Account Verification";
            html = "<div style='border:1px solid ; width:80%;'><div class='leave' style='margin:0 10% 0 10%;'><p>To<br>" + user.UserName + "<br>" + DateTime.Now.ToString("dd MMMM yyyy") + "<br>" +
                           "<p>Thank you for signing up with Sarvamcart. We are delighted to have you with us! Kindly verify your account by clicking the following link: <a href=" + ConfigurationManager.AppSettings["SiteUrl"] + "Account/ConfirmEmail/?token=" + user.Token + "&email=" + user.EmailAddress + "&dtt=" + DateTime.Now.TimeOfDay.ToString() + ">click here </a> </p>" +
                           "</p><br><br> Regards<br> Admin<br>SarvamCart<br><br></div></div>";
            MailService.Send(from, to, subject, html);
        }

        public void SendForgetPasswordEmail(User user)
        {
            var from = "";
            var to = "";
            var subject = "";

            var html = "";
            from = ConfigurationManager.AppSettings["AdminMailID"];
            to = user.EmailAddress;
            subject = "Reset your Password!";
            html = "<div style='border:1px solid ; width:80%;'><div class='leave' style='margin:0 10% 0 10%;'><p>Hi,<br>" + user.UserName + "<br>" + DateTime.Now.ToString("dd MMMM yyyy") + "<br>" +
                           "<p>We got request for reset your account password.</p>" + "<br>" + "<p> Please click on the below link to reset your password</p>" + "<br>" + "<p> <a href=" + ConfigurationManager.AppSettings["SiteUrl"] + "Account/ResetPassword/?id=" + user.Id + "&email=" + user.EmailAddress + "&dtt=" + DateTime.Now.ToString("MM/dd/yyyy") + "> [click here] </a> </p>" +
                           "</p><br><br> Regards<br> Admin<br>SarvamCart<br><br></div></div>";
            MailService.Send(from, to, subject, html);
        }
    }

}
