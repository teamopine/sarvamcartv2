﻿using SarvamCart.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamOpine.Lib.Data;

namespace SarvamCart.Domain.Services
{
   
    public interface ISarvamCartServiceBase<T> : IServiceBase<T> where T : class
    {

    }

    public class SarvamCartServiceBase<T> : ServiceBase<SarvamEntities, T>, ISarvamCartServiceBase<T>
        where T : class
    {
        public SarvamCartServiceBase(SarvamEntities _dbContext) : base(_dbContext)
        {

        }

    }
}
