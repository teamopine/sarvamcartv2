﻿using SarvamCart.Domain.Data;
using SarvamCart.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SarvamCart.Domain.Services
{
    public interface IQuotationService : ISarvamCartServiceBase<Quotation>
    {
       // int Save(int UserId);
    }
    public class QuotationService : SarvamCartServiceBase<Quotation>, IQuotationService
    {
        public SarvamCartServiceBase<Quotation> manageQuotationServices;
        public QuotationService(SarvamEntities context, SarvamCartServiceBase<Quotation> _manageQuotationServices) : base(context)
        {
            manageQuotationServices = _manageQuotationServices;
        }
        //public int Save(int UserId )
        //{
        //    Quotation cartModel = new Quotation();

        //    cartModel.UserId =UserId;
        //    cartModel.CreatedOn = DateTime.Now;
        //    Add(cartModel);
        //    Save();

        //    return cartModel.Id;
        //}

    }
}
