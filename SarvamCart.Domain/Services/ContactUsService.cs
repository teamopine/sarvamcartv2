﻿using SarvamCart.Domain.Data;
using SarvamCart.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SarvamCart.Domain.Services
{
    public interface IContactUsService : ISarvamCartServiceBase<ContactU>
    {
        int Save(ContactUsModel model);
    }
    public class ContactUsService : SarvamCartServiceBase<ContactU>, IContactUsService
    {
        public ContactUsService(SarvamEntities context) : base(context) { }
        //public SarvamCartServiceBase<ContactU> contactUsServices;
        //public ContactUsServices(SarvamEntities context, SarvamCartServiceBase<ContactU> _contactUsServices) : base(context)
        //{
        //    contactUsServices = _contactUsServices;
        //}

        public int Save(ContactUsModel model)
        {
            ContactU contact = new ContactU();
            //try
            //{
                contact.Name = model.Name;
                contact.UserId = model.UserId;
                contact.GuestId = model.GuestId ;
                contact.Email = model.Email;
                contact.Message = model.Message;
                contact.UserRole = model.UserRole;
                contact.ContactNumber = model.ContactNumber;

                Add(contact);
                Save();

            //}
            //catch (Exception)
            //{
            //}
            

            return contact.Id;
        }
    }
}
