﻿using SarvamCart.Domain.Data;
using SarvamCart.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SarvamCart.Domain.Services
{
    public interface IProductService : ISarvamCartServiceBase<Product>
    {
        int Save(ManageProduct model);
        void Delete(int id);
       // List<Product> ProductList(int id);  

    }
    public class ProductService : SarvamCartServiceBase<Product>, IProductService
    {
        public SarvamCartServiceBase<Product> manageProductServices;

        public ProductService(SarvamEntities context, SarvamCartServiceBase<Product> _manageProductServices) : base(context)
        {
            manageProductServices = _manageProductServices;
        }

        public int Save(ManageProduct model)
        {
            Product productModel = new Product();

            if (model.Id > 0)
            {
                    productModel.Id = model.Id ?? 0;
                    productModel.Title = model.Title;
                    productModel.CategoryTypeId = model.CategoryTypeId;
                    productModel.DisplayHome = model.DisplayHome;
                    productModel.Description = model.Description;
                    //productModel.Image = model.Image;
                    productModel.IsActive = model.IsActive;
                    productModel.Price = model.Price;
                    productModel.ProductCode = model.ProductCode;
                    productModel.Stock = model.Stock;

                    Edit(productModel);
                    Save();
                

            }
            else
            {
                    productModel.Title = model.Title;
                    productModel.CategoryTypeId = model.CategoryTypeId;
                    productModel.DisplayHome = model.DisplayHome;
                    productModel.Description = model.Description;
                    //productModel.Image = model.Image;
                    productModel.IsActive = model.IsActive;
                    productModel.Price = model.Price;
                    productModel.ProductCode = model.ProductCode;
                    productModel.Stock = model.Stock;

                    Add(productModel);
                    Save();
               
            }

            return productModel.Id;
        }


        public void Delete(int id)
        {
            var product = FirstOrDefault(x => x.Id == id);
            product.IsDeleted = true;
            Edit(product);
            Save();
        }
        //public List<Product> ProductList(int id)
        //{
        //    return DBContext.Products.Where(x => x.CategoryTypeId == id).ToList();
        //}

        

    }
}
