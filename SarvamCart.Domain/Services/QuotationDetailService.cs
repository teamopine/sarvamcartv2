﻿using SarvamCart.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SarvamCart.Domain.Services
{
    public interface IQuotationDetailService : ISarvamCartServiceBase<QuotationDetail>
    {
       // int Save( int productId);
    }
    public class QuotationDetailService : SarvamCartServiceBase<QuotationDetail>, IQuotationDetailService
    {
        public SarvamCartServiceBase<QuotationDetail> manageQuotationDetailServices;

        public QuotationDetailService(SarvamEntities context, SarvamCartServiceBase<QuotationDetail> _manageQuotationDetailServices) : base(context)
        {
            manageQuotationDetailServices = _manageQuotationDetailServices;
        }

        //public int Save(int productId)
        //{
        //    CartDetail cartDetailModel = new CartDetail();


        //    cartDetailModel.ProductId = productId;
        //    //Add(cartDetailModel);
        //    Save();

        //    return cartDetailModel.Id;
        //}
    }
}
