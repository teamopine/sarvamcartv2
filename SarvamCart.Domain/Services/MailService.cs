﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SarvamCart.Domain.Services
{
   public interface IMailService
    {
        void Send(string from, string to, string subject, string html);
    }
    
    public class MailService: IMailService
    {
        public void Send(string from, string to, string subject, string html)
        {
            var ApiKey = ConfigurationManager.AppSettings["SideGridKey"];
            var client = new SendGridClient(ApiKey);
            var From = new EmailAddress(from);
            var Subject = subject;
            var To = new EmailAddress(to);

            var htmlContent = html;
            var msg = MailHelper.CreateSingleEmail(From, To, Subject, "", htmlContent);
            client.SendEmailAsync(msg);

        }
    }
}
