﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SarvamCart.Domain.Models
{
    public class ManageCategory
    {
        public int? Id { get; set; }
        [Required]
        [StringLength(15, ErrorMessage = "Too many characters.")]
        public string CategoryName { get; set; }
        public decimal DisplayOrder { get; set; }
        public bool IsActive { get; set; }
      //  public bool IsDeleted { get; set; }
    }
}
