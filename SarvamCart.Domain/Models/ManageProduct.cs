﻿using SarvamCart.Domain.Data;
using SarvamCart.Domain.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SarvamCart.Domain.Models
{
   public class ManageProduct
    {
        public ManageProduct()
        {
            CategoryTypes = new List<SelectListItem>();
            InnerImage =new List<ManageProductImages>();
        }
        public int? Id { get; set; }
        // [Required]
        public string Title { get; set; }
        [Required]
        [MinLength(20, ErrorMessage = "Minimum 20 characters required")]
        public string Description { get; set; }
        //  [Required]
        public decimal? Price { get; set; }
        //public string Image { get; set; }
        public int? CategoryTypeId { get; set; }
        public string CategoryType { get; set; }
        public bool IsActive { get; set; }
        [Required]
        public string ProductCode { get; set; }
       [Required]
        public int? Stock { get; set; }
        public bool DisplayHome { get; set; }

        public IEnumerable<SelectListItem> CategoryTypes { get; set; }
       
        public List<ManageProductImages> InnerImage { get; set; }
        public List<Product> RelatedProduct { get; set; }
    }
}
