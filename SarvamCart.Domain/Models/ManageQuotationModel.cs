﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SarvamCart.Domain.Models
{
    public class ManageQuotationModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int StatusOfOrder { get; set; }
        public DateTime CreatedOn { get; set; }

    }
}
