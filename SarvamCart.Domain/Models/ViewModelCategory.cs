﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SarvamCart.Domain.Models
{
    public class ViewModelCategory
    {
        public int? Id { get; set; }
        public string CategoryName { get; set; }
        public decimal DisplayOrder { get; set; }
        public bool IsActive { get; set; }
    }
}
