﻿using SarvamCart.Domain.Data;
using SarvamCart.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SarvamCart.Domain.Models
{
    public class ViewModelProduct
    {
        public ViewModelProduct()
        {
            CategoryTypes = new List<SelectListItem>();
            InnerImage = new List<ProductImage>();
        }
        public int? Id { get; set; }
        // [Required]
        public string Title { get; set; }
        // [Required]
        public string Description { get; set; }
        //  [Required]
        public decimal? Price { get; set; }
        // public string Image { get; set; }
        public int? CategoryTypeId { get; set; }
        public bool IsActive { get; set; }
        public string ProductCode { get; set; }
        public int? Stock { get; set; }
        public bool DisplayHome { get; set; }
        public string Image { get; set; }
        public string Category { get; set; }

        public List<SelectListItem> CategoryTypes { get; set; }

        public List<ProductImage> InnerImage { get; set; }
    }
}
