﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SarvamCart.Domain.Models
{
    public class ViewModelQuotaion
    {
        public ViewModelQuotaion()
        {
            CartDetails = new List<ManageQuotaionDetailModel>();
        }
        public int Id { get; set; }
        public int UserId { get; set; }
        [DataType(DataType.Date), DisplayFormat(DataFormatString = @"{0:dd\/MM\/yyyy HH:mm}",
           ApplyFormatInEditMode = true)]
        public DateTime CreatedOn { get; set; }
        public int StatusOfOrder { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string ContactNumber { get; set; }
        public decimal GrantTotal { get; set; }
        //public string Address { get; set; }
        public List<ManageQuotaionDetailModel> CartDetails { get; set; }
    }
}
