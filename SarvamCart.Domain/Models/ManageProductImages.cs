﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SarvamCart.Domain.Services
{
    public class ManageProductImages
    {
        public int? Id { get; set; }
        public int ProductId { get; set; }
        public string ImageName { get; set; }
        public bool IsDeleted { get; set; }
       

    }
}
