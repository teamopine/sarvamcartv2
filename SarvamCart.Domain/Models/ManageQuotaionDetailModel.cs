﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SarvamCart.Domain.Models
{
    public class ManageQuotaionDetailModel
    {
        public int Id { get; set; }
        public int QuotationId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public decimal Quentity { get; set; }
        public string ProductImage { get; set; }
        public decimal FinalPrice { get; set; }

    }
}
